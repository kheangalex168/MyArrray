//
//  CollectionViewCell.swift
//  MyArrray
//
//  Created by Kheang on 7/4/22.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var kidCountLabel: UILabel!
    
//    override var isSelected: Bool {
//        didSet {
//            self.backgroundColor = isSelected ? .red : .white
//        }
//    }
    
    func configCollectionViewCell(numberOfKidsRec: CustomMyInfo.NumberOfKids.object) {
        kidCountLabel.text = numberOfKidsRec.count
    }
}

