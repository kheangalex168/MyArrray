//
//  ViewModel.swift
//  MyArrray
//
//  Created by Kheang on 6/4/22.
//

import Foundation

class ProfileInfoDataVM {
    
    var myInfoData          = [CustomMyInfoData<Any>]()
    var dateAndTimeRec      : CustomMyInfo.DateAndTime?
    var contactRec          : CustomMyInfo.DateAndTime?
    var numberOfKidsRec     : CustomMyInfo.NumberOfKids?
    
    func getMyInfoData() {
        // MARK: -  Init DateAndTime -
        dateAndTimeRec = CustomMyInfo.DateAndTime(title  : "DATE AND TIME",
                                          object : [
                                            CustomMyInfo.DateAndTime.object(image: "date", placeholder: "Date you need babaysitter"),
                                            CustomMyInfo.DateAndTime.object(image: "clock", placeholder: "From"),
                                          ])
        // MARK: -  Init NumberOfKids -
        numberOfKidsRec = CustomMyInfo.NumberOfKids(title  : "NUMBER OF KIDS",
                                                    object : [
                                                      CustomMyInfo.NumberOfKids.object(image: "", count: "1 Kid"),
                                                      CustomMyInfo.NumberOfKids.object(image: "", count: "2 Kids"),
                                                      CustomMyInfo.NumberOfKids.object(image: "", count: "3 Kids"),
                                                      CustomMyInfo.NumberOfKids.object(image: "", count: "4 Kids"),
                                                      CustomMyInfo.NumberOfKids.object(image: "", count: "5 Kids"),
                                                    ])
        // MARK: -  Init Contact -
        contactRec = CustomMyInfo.DateAndTime(title  : "CONTACT",
                                          object : [
                                            CustomMyInfo.DateAndTime.object(image: "location", placeholder: "Address"),
                                            CustomMyInfo.DateAndTime.object(image: "phone", placeholder: "Phone Number"),
                                          ])
        // MARK: - Assign all data  -
        myInfoData = [
            CustomMyInfoData(title: MyInfoSection.DateAndTime.rawValue,  value: dateAndTimeRec),
            CustomMyInfoData(title: MyInfoSection.NumberOfKids.rawValue, value: numberOfKidsRec),
            CustomMyInfoData(title: MyInfoSection.Contact.rawValue,      value: contactRec),
        ]
    }
}
