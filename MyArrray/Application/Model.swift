//
//  Model.swift
//  MyArrray
//
//  Created by Kheang on 6/4/22.
//

import Foundation

// For title management
enum MyInfoSection : String{
    case DateAndTime
    case NumberOfKids
    case Contact
}


struct CustomMyInfoData<T> {
    var title  : String   // for section
    var value  : T?       // data in each section
}

struct CustomMyInfo {
    
    // DateAndTime
    struct DateAndTime {
        
        var title  : String  // for header title
        var object : [object]
        
        struct object {
            let image : String?
            let placeholder : String?
        }
    }
    
    // NumberOfKids
    struct NumberOfKids {
        var title    : String
        var object   : [object]

        struct object {
            let image  : String?
            let count  : String?
        }
    }
    
//    // Receipt
//    struct CancelReceipt {
//        var title    : String
//        var object   : [object]
//
//        struct object {
//            var name         : String?
//            var price        : String?
//            var date         : String?
//            var id           : String?
//            var objectDetail : objectDetail
//
//            struct objectDetail {
//                var approval_number : String?
//                var supply_amount   : String?
//                var vat             : String?
//            }
//        }
//    }
    
}
