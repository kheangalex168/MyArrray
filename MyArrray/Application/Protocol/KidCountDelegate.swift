//
//  KidCountProtocol.swift
//  MyArrray
//
//  Created by Kheang on 8/4/22.
//

import Foundation

protocol KidCountDelegate {
    func getKidCount(count: String)
}
