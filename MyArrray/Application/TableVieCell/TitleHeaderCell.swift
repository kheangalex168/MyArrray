//
//  TitleHeaderCell.swift
//  MyArrray
//
//  Created by Kheang on 6/4/22.
//

import UIKit

class TitleHeaderCell: UITableViewCell {

    @IBOutlet weak var titleHeaderLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configHeaderCell(headerTitle: String) {
        titleHeaderLabel.text = headerTitle
    }

}
