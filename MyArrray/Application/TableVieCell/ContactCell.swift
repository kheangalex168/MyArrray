//
//  ContactCell.swift
//  MyArrray
//
//  Created by Kheang on 6/4/22.
//

import UIKit

class ContactCell: UITableViewCell, UITextFieldDelegate{
    
    @IBOutlet weak var iconImage: UIImageView!
    
    @IBOutlet weak var tf: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tf.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func modifiedTextField() {
        tf.attributedPlaceholder = NSAttributedString(string: "Helo", attributes: [
            .foregroundColor: UIColor.lightGray,
            .font: UIFont.boldSystemFont(ofSize: 14.0)
        ])
    }
    
    func configContactCell(contactRec: CustomMyInfo.DateAndTime.object) {
        iconImage.image = UIImage(named: contactRec.image ?? "")
        tf.placeholder = contactRec.placeholder
    }
     
}


