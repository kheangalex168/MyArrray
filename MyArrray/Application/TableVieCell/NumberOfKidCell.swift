//
//  NumberOfKidCell.swift
//  MyArrray
//
//  Created by Kheang on 7/4/22.
//

import UIKit

class NumberOfKidCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - variable & properties
    var myInfoVM = ProfileInfoDataVM()
    
    var delegate: KidCountDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        myInfoVM.getMyInfoData()
        
        print("==>myInfoData", myInfoVM.myInfoData)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension NumberOfKidCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        myInfoVM.numberOfKidsRec?.object.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
        if cell.isSelected {
            cell.contentView.backgroundColor = UIColor.red
        }else {
            cell.contentView.backgroundColor = UIColor.white
        }
        
        if let rec = myInfoVM.numberOfKidsRec?.object {
            cell.configCollectionViewCell(numberOfKidsRec: rec[indexPath.row])
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedCell = collectionView.cellForItem(at: indexPath)  {
            selectedCell.contentView.backgroundColor = UIColor.red
            selectedCell.isSelected = true
            if let count = myInfoVM.numberOfKidsRec?.object[indexPath.row].count {
                delegate?.getKidCount(count: count)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let deSelectedCell:UICollectionViewCell = collectionView.cellForItem(at: indexPath) {
            deSelectedCell.contentView.backgroundColor = UIColor.white
            deSelectedCell.isSelected = false
        }
    }
}
