//
//  RequestDetail.swift
//  MyArrray
//
//  Created by Kheang on 8/4/22.
//

import Foundation

struct RequestDetail {
    var title: [String]
    var detail: [String]
}
