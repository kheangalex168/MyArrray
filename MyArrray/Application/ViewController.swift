//
//  ViewController.swift
//  MyArrray
//
//  Created by Kheang on 6/4/22.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var tableView   : UITableView!
    
    //MARK: - variable & properties
    var myInfoVM = ProfileInfoDataVM()
    let controller = NumberOfKidCell()
    
    
    var date = ""
    var time = ""
    var location = ""
    var phone = ""
    var countKid = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        configTableView()
        
        myInfoVM.getMyInfoData()
        
        print("==>myInfoData", myInfoVM.myInfoData)
        
    }
    
    func configTableView(){
        // Disable Tableview Top Only
        tableView.bounces               = false
        tableView.alwaysBounceVertical  = false
        
        // Space between tableView & LogOutView
        self.tableView.contentInset     = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        
        // create the alert
        let alert = UIAlertController(title: "My Title", message: "\(date) \n  \(time) \n \(countKid) \n \(location) \n \(phone)", preferredStyle: UIAlertController.Style.alert)

        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController: KidCountDelegate {
    func getKidCount(count: String) {
        self.countKid = count
    }
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension ViewController: UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return myInfoVM.myInfoData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // row of title & object value
        let section = MyInfoSection(rawValue: myInfoVM.myInfoData[section].title)
        
        switch section {
        
        case .DateAndTime:
            return (myInfoVM.dateAndTimeRec?.object.count ?? 0) + 1 // for header title
            
        case .NumberOfKids:
//            return (myInfoVM.numberOfKidsRec? ?? 0) + 1 // for header title
            return 2
        
        case .Contact:
            return (myInfoVM.contactRec?.object.count ?? 0) + 1 // for header title
        
        default:
            return 0
        }
    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = MyInfoSection(rawValue: myInfoVM.myInfoData[indexPath.section].title)
        
        switch section {
        
        case .DateAndTime:
            if indexPath.row == 0 {
                let headerTitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell") as! TitleHeaderCell
                headerTitleCell.configHeaderCell(headerTitle: myInfoVM.dateAndTimeRec?.title ?? "")
                return headerTitleCell
            }else {
                let dateAndTimeCell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
                dateAndTimeCell.tf.tag = indexPath.row - 1
                dateAndTimeCell.tf.delegate = self
                if let rec = myInfoVM.dateAndTimeRec?.object {
                    dateAndTimeCell.configContactCell(contactRec: rec[indexPath.row - 1])
                }
                return dateAndTimeCell
            }
            
        case .NumberOfKids:
            if indexPath.row == 0 {
                let headerTitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell") as! TitleHeaderCell
                headerTitleCell.configHeaderCell(headerTitle: myInfoVM.numberOfKidsRec?.title ?? "")
                return headerTitleCell
            }else {
                let numberOfKidsCell = tableView.dequeueReusableCell(withIdentifier: "NumberOfKidCell") as! NumberOfKidCell
                numberOfKidsCell.delegate = self
                return numberOfKidsCell
            }
            
        case .Contact:
            if indexPath.row == 0 {
                let headerTitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderCell") as! TitleHeaderCell
                headerTitleCell.configHeaderCell(headerTitle: myInfoVM.contactRec?.title ?? "")
                return headerTitleCell
            }else {
                let contactCell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
                contactCell.tf.tag = indexPath.row + 1
                contactCell.tf.delegate = self
                if let rec = myInfoVM.contactRec?.object {
                    contactCell.configContactCell(contactRec: rec[indexPath.row - 1])
                }
                return contactCell
            }
            
        default:
            return UITableViewCell()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        let kActualText = (textField.text ?? "") + string
        print(kActualText)
        switch textField.tag
            {
            case 0:
               date = kActualText;
            case 1:
               time = kActualText;
            case 2:
               location = kActualText;
            case 3:
               phone = kActualText;
            default:
                print("It is nothing");
            }
            return true
    }

}

